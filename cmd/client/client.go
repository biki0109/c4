package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/c4/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type C4Client struct {
	pb.C4Client
}

func NewC4Client(address string) *C4Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial C4 service", l.Error(err))
	}

	c := pb.NewC4Client(conn)

	return &C4Client{c}
}
