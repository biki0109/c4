// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/c4/internal/services"
	"git.begroup.team/platform-transport/c4/internal/stores"
	"git.begroup.team/platform-transport/c4/pb"
	"honnef.co/go/tools/config"
)

func registerService(cfg *config.Config) pb.C4Server {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
